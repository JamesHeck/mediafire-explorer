# MediaFire-Explorer
client to manage your files in mediafire

* software to manage your files in mediafire
* copy files folders
* move files folders
* rename files folders
* delete files folders
* upload download files folders
* share files folders
* list shared files folders

`## Download:`
[https://github.com/jamesheck2019/MediaFire-Explorer/releases](https://github.com/jamesheck2019/MediaFire-Explorer/releases)
* .
![https://i.postimg.cc/nLNskbZg/ure-Wiz131.png](https://i.postimg.cc/nLNskbZg/ure-Wiz131.png)
